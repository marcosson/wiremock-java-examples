# README #
WireMock is a flexible API mocking tool for fast, robust and comprehensive testing.

### What is this repository for? ###

* Examples on how to use WireMock API for Java and JUnit
* Examples on how to build a mock for Jenkins' services

### How do I get set up? ###

The repository is ready to be cloned and run, it doesn't require additional steps.

### Notes about Jenkins' mock ###

There are two versions of Jenkins' mock:

* BasicJenkins.java contains a minimum number of services and no stateful behavior.

* AdvancedJenkins.java contains a more complete Jenkins' mock, with almost all the services offered by the "real" Jenkins, some of the services have a stateful behavior too.

AdvancedJenkins' stateful behavior should be adapted to the user's needs. This behavior can be "circular" or reach a dead end.

Circular behavior: State#1 --[POST]-> State#2 --[GET]-> State#1 --[POST]-> State#2 -> ...

Dead end behavior: State#1 --[POST]-> State#2

In short: a circular behavior lets the user "restart" the scenario, while the dead end one stops at the second state forever.
What does this mean, practically?

Circular behavior:

* Start from the initial scenario

* A GET request will show the initial scenario (and so do subsequent GET request, until the first POST request)

* A POST request triggers the state change

* A GET request will now show the final scenario and at the same time trigger the state change to reset the scenario

At this point, a GET request will show the initial scenario (because the state has been reset) and a POST request will go through.

Dead end behavior:

* Start from the initial scenario

* A GET request will show the initial scenario (and so do subsequent GET requests, until the first POST request)

* A POST request triggers the state change

* A GET request will now show the final scenario (and so do subsequent GET requests)

The difference here is that the state doesn't get reset, meaning that every GET request will now show the final scenario and POST requests won't go through because the state is set in the last scenario.

It's up to the user to decide wich one of the two versions of a stateful behavior better fits its needs. To switch from the circular behavior (the one used in the examples) to the dead end one it's necessary to remove the `.willSetStateTo(Scenario.STARTED)` instruction written at the end of stateful mocks (re-adding it, obviously, will switch from the dead end behavior to the circular one).

### Who do I talk to? ###

* Repo owner or admin