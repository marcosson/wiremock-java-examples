package jenkins;

/*
 * Minimum example to mock Jenkins' services
 * The services mocked here give the responses needed to execute
 * CFE tests (on 16/01/2018) and nothing more
 */

import com.github.tomakehurst.wiremock.WireMockServer;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class BasicJenkins {

	public static void main(String[] args) {
		
		WireMockServer wireMockServer = new WireMockServer(options().port(8080));
		/*
		 * The server is started but never stopped
		 * This way, it remains up and can be accessed from other sources (in this
		 * case, tests that run on another project)
		 */
		wireMockServer.start();
		
		/*
		 * Service that returns info about a (generic) job
		 */
		stubFor(get(urlPathMatching("/job/(.*?)/api/json"))
				.willReturn(aResponse()
						.withStatus(200)
						.withBodyFile("gmail-forTestOnCute.json")
						.withHeader("Content-Type", "application/json;charset=utf-8")));
		
		/*
		 * Service that returns info about a (generic) job's build
		 */
		stubFor(get(urlPathMatching("/job/(.*?)/([0-9]*)/api/json"))
				.willReturn(aResponse()
						.withStatus(200)
						.withBodyFile("body-for-cute-getBuildDetails.json")
						.withHeader("Content-Type", "application/json;charset=utf-8")));
		
		/*
		 * Service that returns a (generic) job's configuration file
		 */
		stubFor(get(urlPathMatching("/job/(.*?)/config.xml"))
				.willReturn(aResponse()
						.withStatus(200)
						.withBodyFile("body-for-cute-getAllProjectsConfiguration.xml")
						.withHeader("Content-Type", "text/xml")));
		
		/*
		 * Service that returns info about all the jobs contained in Jenkins
		 */
		stubFor(get(urlPathEqualTo("/api/json"))
				.willReturn(aResponse()
						.withStatus(200)
						.withBodyFile("body-for-cute-getAllJobs.json")
						.withHeader("Content-Type", "application/json;charset=utf-8")));
	}

}
