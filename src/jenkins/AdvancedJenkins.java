package jenkins;

/*
 * Extended example that mocks more Jenkins' services, not only
 * the ones needed for the tests but also more advanced service
 * like building/deleting/adding a job using WireMock's stateful
 * behavior
 */

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.stubbing.Scenario;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class AdvancedJenkins {

	public static void main(String[] args) {
		
		WireMockServer wireMockServer = new WireMockServer(options().port(8080));
		wireMockServer.start();
		
		/*
		 * Authentication service
		 */ 
		stubFor(get(urlEqualTo("/crumbIssuer/api/json"))
				.willReturn(aResponse()
						.withStatus(200)
						.withHeader("Content-Type", "application/json;charset=utf-8")
						.withBody("{\"_class\":\"hudson.security.csrf.DefaultCrumbIssuer\","
								+ "\"crumb\":\"74c509197a90fffef1485ee36e2945ea\","
								+ "\"crumbRequestField\":\"Jenkins-Crumb\"}")));
		
		/*
		 * Log service
		 */
		stubFor(post(urlPathMatching("/job/(.*?)/([0-9]*)/logText/progressiveText"))
				.willReturn(aResponse()
						.withStatus(200)
						.withHeader("Content-Type", "text/plain;charset=utf-8")
						.withBody("Avviato dall'utente Marco Passon\r\n"
								+ "Compilazione in corso nello spazio di lavoro "
								+ "C:\\Users\\Marco\\.jenkins\\workspace\\gmail-forTestOnCute\r\n"
								+ "Triggering gmail-forTestOnCute � default\r\n"
								+ "gmail-forTestOnCute � default completed with result SUCCESS\r\n"
								+ "Finished: SUCCESS\r\n")));
		
		/*
		 * Service that returns the configuration file of a job
		 */
		stubFor(get(urlPathMatching("/job/(.*?)/config.xml"))
				.willReturn(aResponse()
						.withStatus(200)
						.withBodyFile("body-for-cute-getAllProjectsConfiguration.xml")
						.withHeader("Content-Type", "text/xml")));
		
		/*
		 * Service that enables a job
		 */
		stubFor(post(urlEqualTo("/job/gmail-gonnaGetDeleted/enable"))
				.willReturn(aResponse()
						.withStatus(302)
						.withHeader("Location", "http://localhost:8080/job/gmail-gonnaGetDeleted/")));
		
		/*
		 * Service that disables a job
		 */
		stubFor(post(urlEqualTo("/job/gmail-gonnaGetDeleted/disable"))
				.willReturn(aResponse()
						.withStatus(302)
						.withHeader("Location", "http://localhost:8080/job/gmail-gonnaGetDeleted/")));
		
		/*
		 * Service that checks if a job exists
		 */
		stubFor(head(urlEqualTo("/job/gmail-forTestOnCute/api/json"))
				.willReturn(aResponse().withStatus(200)));
		
		/*
		 * Service that returns info about a (generic) job's build
		 */
		stubFor(get(urlPathMatching("/job/(.*?)/([0-9]*)/api/json"))
				.willReturn(aResponse()
						.withStatus(200)
						.withBodyFile("body-job-name-api-json-initial.json")
						.withHeader("Content-Type", "application/json;charset=utf-8")));
		
		/*
		 * Queue service
		 */
		/*
		stubFor(get(urlEqualTo("/queue/api/json"))
				.willReturn(aResponse()
						.withStatus(200)
						.withHeader("Content-Type", "application/json;charset=utf-8")
						.withBody("{\"_class\":\"hudson.model.Queue\","
								+ "\"discoverableItems\":[],\"items\":[]}")));
		*/
		
		/*
		 * Queue service (more meaningful, response taken from Jenkins' documentation)
		 */
		stubFor(get(urlEqualTo("/queue/api/json"))
				.willReturn(aResponse()
						.withStatus(200)
						.withHeader("Content-Type", "application/json;charset=utf-8")
						.withBodyFile("queue-list-example.json")));
		
		
		/*
		 * Service that returns info about all the jobs in Jenkins
		 * Note: this stub serves as starting point for two stateful behaviors 
		 * (create job and delete job) but it doesn't need a scenario and
		 * a state itself because the "real" stateful behavior starts with the
		 * POST request
		 */
		stubFor(get(urlEqualTo("/api/json"))
				.willReturn(aResponse()
						.withStatus(200)
						.withHeader("Content-Type", "application/json;charset=utf-8")
						.withBodyFile("body-api-json-initial.json")));
		
		/*
		 * Stateful behavior to create a new job
		 * Stub that handles the POST request sent when creating a new job from API
		 */
		stubFor(post(urlEqualTo("/createItem?name=gmail-createdFromAPI"))
				.inScenario("Add job scenario")
				.whenScenarioStateIs(Scenario.STARTED)
				.willReturn(aResponse().withStatus(201))
				.willSetStateTo("New job added"));
		
		/*
		 * Stateful behavior to create a new job
		 * Stub that returns info about all the jobs after the insertion of a new job
		 */
		stubFor(get(urlEqualTo("/api/json"))
				.inScenario("Add job scenario")
				.whenScenarioStateIs("New job added")
				.willReturn(aResponse()
						.withStatus(200)
						.withHeader("Content-Type", "application/json;charset=utf-8")
						.withBodyFile("body-api-json-insertion.json"))
				.willSetStateTo(Scenario.STARTED));
		
		/*
		 * Stateful behavior to delete a job
		 * Stub that handles the POST request sent when deleting a job from API
		 */
		stubFor(post(urlEqualTo("/job/gmail-gonnaGetDeleted/doDelete"))
				.inScenario("Delete job scenario")
				.whenScenarioStateIs(Scenario.STARTED)
				.willReturn(aResponse().withStatus(302))
				.willSetStateTo("Job deleted"));
		
		/*
		 * Stateful behavior to delete a job
		 * Stub that returns info about all the jobs after the deletion of a new job
		 */
		stubFor(get(urlEqualTo("/api/json"))
				.inScenario("Delete job scenario")
				.whenScenarioStateIs("Job deleted")
				.willReturn(aResponse()
						.withStatus(200)
						.withHeader("Content-Type", "application/json;charset=utf-8")
						.withBodyFile("body-api-json-deleted.json"))
				.willSetStateTo(Scenario.STARTED));
		
		/*
		 * Stateful behavior to build a job
		 * Initial stub that returns info about a single job
		 */
		stubFor(get(urlPathMatching("/job/(.*?)/api/json"))
				.inScenario("Build job scenario")
				.whenScenarioStateIs(Scenario.STARTED)
				.willReturn(aResponse()
						.withStatus(200)
						.withHeader("Content-Type", "application/json;charset=utf-8")
						.withBodyFile("body-job-name-api-json-initial.json")));
		
		/*
		 * Stateful behavior to build a job
		 * Stub that handles the POST request sent when building a job from API
		 */
		stubFor(post(urlPathMatching("/job/(.*?)/build"))
				.inScenario("Build job scenario")
				.whenScenarioStateIs(Scenario.STARTED)
				.willReturn(aResponse()
						.withStatus(201)
						.withHeader("Location", "http://localhost:8080/queue/item/11/"))
				.willSetStateTo("Job built"));
		
		/*
		 * Stateful behavior to build a job
		 * Stub that returns info about a single job after building it
		 */
		stubFor(get(urlPathMatching("/job/(.*?)/api/json"))
				.inScenario("Build job scenario")
				.whenScenarioStateIs("Job built")
				.willReturn(aResponse()
						.withStatus(200)
						.withHeader("Content-Type", "application/json;charset=utf-8")
						.withBodyFile("body-job-name-api-json-built.json"))
				.willSetStateTo(Scenario.STARTED));
	}

}
