package basic;

/*
 * Simple WireMock example to show how to set up a mock outside JUnit
 * This example sets up a mock and then checks that its response is
 * a specific string, then it stops the mock
 */

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

import com.github.tomakehurst.wiremock.WireMockServer;

public class ProgrammaticallyManagedServer {
	
	public static void main(String[] args) throws IOException {
		/*
		 * WireMockServer's constructor accepts an Option object as parameter
		 * Option is a class that lets the user specify single options instead
		 * of specifying all of them singularly
		 * In this case, asking for the WireMockServer to start at port 8080 is
		 * unnecessary since it starts on port 8080 by default 
		 */
		WireMockServer wireMockServer = new WireMockServer(wireMockConfig().port(8080));
		
		wireMockServer.start();
		
		/*
		 * Configure the static client for an alternative host and port
		 * Unnecessary too since these are its default host and port
		 */
		configureFor("localhost", wireMockServer.port());
		
		/*
		 * Method to set up a stub for the localhost:8080/baeldung URL that returns
		 * just a string as body
		 */
		stubFor(get(urlEqualTo("/baeldung"))
				.willReturn(aResponse()
						.withBody("Welcome to Baeldung!")));
		
		/*
		 * Instructions to create a basic HTTP client in Java, send a GET
		 * request to http://localhost:8080/baeldung and check that its
		 * response's body is equal to "Welcome to Baeldung!"
		 */
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		HttpGet request = new HttpGet("http://localhost:8080/baeldung");
		HttpResponse httpResponse = httpClient.execute(request);
		
		String reponseString = convertResponseToString(httpResponse);
		
		verify(getRequestedFor(urlEqualTo("/baeldung")));
		assertEquals("Welcome to Baeldung!", reponseString);
	
		wireMockServer.stop();
	}
	
	private static String convertResponseToString(HttpResponse response) throws IOException {
	    InputStream responseStream = response.getEntity().getContent();
	    Scanner scanner = new Scanner(responseStream, "UTF-8");
	    String responseString = scanner.useDelimiter("\\Z").next();
	    scanner.close();
	    return responseString;
	}

}
