package basic;

/*
 * Simple WireMock example to show how to set up a stateful mock
 * This example uses JUnit, but the way the mock is set up can be used
 * outside the JUnit environment without any change
 */

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

import static io.restassured.RestAssured.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.stubbing.Scenario;

public class StatefulBehaviour {
	
	/*
	 * JUnit needs an instance of WireMockRule instead of the usual WireMockServer
	 * Setting the second parameter to "false" prevents WireMock from warning the user
	 * about possible errors in the URL and just gives an error instead
	 */
	@Rule
	public WireMockRule wm = new WireMockRule(options().port(8090), false);
	
	/*
	 * Method to set up a stub, it gets called before starting the tests thanks to the
	 * @Before annotation
	 * Its' body can be used even outside JUnit, it's the standard way to build a
	 * stateful mock programmatically 
	 */
	@Before
	public void setup() {
		/*
		 * Initial stub for the localhost:8080/todo/items URL
		 * It returns an xml body, depicting a list of things to do and containing
		 * only one item
		 */
		stubFor(get(urlEqualTo("/todo/items")).inScenario("To do list")
				.whenScenarioStateIs(Scenario.STARTED)
				.willReturn(aResponse()
						.withHeader("Content-type", "application/xml")
						.withBody("<items><item>Buy milk</item></items>")));
		
		/*
		 * Stub that triggers the change in the scenario's state if the POST
		 * request's body at the localhost:8080/todo/items URL contains the
		 * "Cancel newspaper subscription" body
		 */
		stubFor(post(urlEqualTo("/todo/items")).inScenario("To do list")
				.whenScenarioStateIs(Scenario.STARTED)
				.withRequestBody(containing("Cancel newspaper subscription"))
				.willReturn(aResponse().withStatus(201))
				.willSetStateTo("Cancel newspaper item added"));
		
		/*
		 * Stub that handles the GET requests at localhost:8080/todo/items
		 * after the scenario's state has changed
		 * The list of things now contains two items
		 */
		stubFor(get(urlEqualTo("/todo/items")).inScenario("To do list")
				.whenScenarioStateIs("Cancel newspaper item added")
				.willReturn(aResponse()
						.withHeader("Content-type", "application/xml")
						.withBody("<items>" +
								"    <item>Buy milk</item>" +
								"    <item>Cancel newspaper subscription</item>" +
								"</items>")));
	}
	
	/*
	 * Test to check that the mock exists and initially contains a list with
	 * only one item initially, then, after a POST request with the proper body,
	 * the mock returns a list with two items in it
	 * The instructions used come from the RestAssured library
	 */
	@Test
	public void toDoListScenario() {
		given()
		.when()
			.get("http://localhost:8090/todo/items")
		.then()
			.assertThat()
			.statusCode(200)
			.and()
			.assertThat().body("items", org.hamcrest.Matchers.contains("Buy milk"))
			.and()
			.assertThat().body("items", org.hamcrest.Matchers.not(org.hamcrest.Matchers.contains("Cancel newspaper subscription")));
		
		given()
			.body("Cancel newspaper subscription")
		.when()
			.post("http://localhost:8090/todo/items")
		.then()
			.assertThat()
			.statusCode(201);
		
		given()
		.when()
			.get("http://localhost:8090/todo/items")
		.then()
			.assertThat()
			.statusCode(200)
			.and()
			.assertThat().body("items.item[0]", org.hamcrest.Matchers.is("Buy milk"))
			.and()
			.assertThat().body("items.item[1]", org.hamcrest.Matchers.is("Cancel newspaper subscription"));
	}

}
