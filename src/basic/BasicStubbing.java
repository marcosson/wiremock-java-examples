package basic;

/*
 * Simple WireMock example to show how to set up a mock
 * This example uses JUnit, but the way the mock is set up can be used
 * outside the JUnit environment without any change
 */

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

import static io.restassured.RestAssured.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.github.tomakehurst.wiremock.junit.WireMockRule;

public class BasicStubbing {
	
	/*
	 * JUnit needs an instance of WireMockRule instead of the usual WireMockServer
	 * Setting the second parameter to "false" prevents WireMock from warning the user
	 * about possible errors in the URL and just gives an error instead
	 */
	@Rule
	public WireMockRule wm = new WireMockRule(options().port(8090), false);
	
	/*
	 * Method to set up a stub for the localhost/some/thing URL
	 * It gets called before starting the tests thanks to the @Before annotation
	 * Its' body can be used even outside JUnit, it's the standard way to build a mock
	 * programmatically 
	 */
	@Before
	public void setUp() {
		stubFor(get(urlEqualTo("/some/thing"))
				.willReturn(aResponse()
						.withHeader("Content-type", "text/plain")
						.withBody("Hello World!")));
	}
	
	/*
	 * Test to check that the mock exists and returns an OK HTTP status when it receives
	 * a GET request
	 * The instructions used come from the RestAssured library
	 */
	@Test
	public void testStatusCodePositive() {
		given()
		.when()
			.get("http://localhost:8090/some/thing")
		.then()
			.assertThat().statusCode(200);		
	}
	
	/*
	 * Test to check that sending a request to a page not "covered" by the mock results in
	 * a 404 error (not found)
	 */
	
	@Test
	public void testStatusCodeNegative() {
		given()
		.when()
			.get("http://localhost:8090/some/thing/else")
		.then()
			.assertThat().statusCode(404);		
	}

}
